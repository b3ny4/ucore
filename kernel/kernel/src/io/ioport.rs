extern {
    fn ioport_in(port: u16) -> u8;
    fn ioport_out(port: u16, data: u8);
}

pub struct IOPort {
    pub address: u16,
}

impl IOPort {

    pub fn new(address: u16) -> Self {
        Self { address }
    }

    pub fn recv(&self) -> u8 {
        let result: u8;
        unsafe {
            result = ioport_in(self.address);
        }
        return result;
    }

    pub fn send(&self, data: u8) {
        unsafe {
            ioport_out(self.address, data);
        }
    }

}