
use core::fmt;
use lazy_static::lazy_static;
use spin::Mutex;

use super::ioport::IOPort;


const SCREEN_WIDTH  : usize = 80;
const SCREEN_HEIGHT : usize = 25;

#[derive(Clone, Copy)]
#[repr(u8)]
pub enum Color {
    Black = 0,
    Blue = 1,
    Green = 2,
    Cyan = 3,
    Red = 4,
    Magenta = 5,
    Brown = 6,
    LightGrey = 7,
    DarkGrey = 8,
    LightBlue = 9,
    LightGreen = 10,
    LightCyan = 11,
    LightRed = 12,
    LightMagenta = 13,
    Yellow = 14,
    White = 15,
}

#[derive(Clone, Copy)]
#[repr(C, packed)]
pub struct Attribute(u8);

impl Attribute {
    pub fn new(fg: Color, bg: Color) -> Self {
        Self((bg as u8) << 4 | (fg as u8))
    }
}

#[derive(Clone, Copy)]
#[repr(C, packed)]
struct Symbol {
    character: u8,
    attribute: Attribute,
}

impl Symbol {
    pub fn new(character: char, attribute: Attribute) -> Self {
        Self {
            character: character as u8,
            attribute
        }
    }
}

#[repr(C, packed)]
struct Buffer {
    symbols: [[Symbol; SCREEN_WIDTH]; SCREEN_HEIGHT]
}

// TODO: enable use of hardware cursor!
struct Cursor {
    x: usize,
    y: usize,
}

impl Cursor {

    fn enable_hw_cursor() {
        let index_register: IOPort = IOPort::new(0x3D4);
        let data_register: IOPort = IOPort::new(0x3D5);

        index_register.send(0x0A);
        data_register.send(data_register.recv() & 0xC0 | 0);

        index_register.send(0x0B);
        data_register.send(data_register.recv() & 0xE0 | (SCREEN_WIDTH as u8 - 1));
    }

    fn set_hw_pos(x:usize, y: usize) {
        if x >= SCREEN_WIDTH || y >= SCREEN_HEIGHT {
            return;
        }

        let pos: usize = y * SCREEN_WIDTH + x;

        let index_register: IOPort = IOPort::new(0x3D4);
        let data_register: IOPort = IOPort::new(0x3D5);

        index_register.send(0x0F);
        data_register.send((pos & 0xFF) as u8);
        index_register.send(0x0E);
        data_register.send(((pos>>8) & 0xFF) as u8);
    }

    fn new() -> Self {
        Cursor::enable_hw_cursor();
        Cursor::set_hw_pos(0, 0);

        Self {
            x: 0,
            y: 0
        }
    }

    fn set_pos(&mut self, x:usize, y: usize) {
        self.x = x;
        self.y = y;

        Cursor::set_hw_pos(self.x, self.y);
    }

    fn move_right(&mut self) -> bool {
        let mut overflow: bool = false;
        self.x += 1;
        if self.x >= SCREEN_WIDTH {
            self.x = 0;
            self.y += 1;
        }
        if self.y >= SCREEN_HEIGHT {
            self.y = SCREEN_HEIGHT - 1;
            overflow = true;
        }
        Cursor::set_hw_pos(self.x, self.y);
        return overflow;
    }

    fn newline(&mut self) -> bool {
        let mut overflow: bool = false;
        self.x = 0;
        self.y += 1;
        if self.y >= SCREEN_HEIGHT {
            self.y = SCREEN_HEIGHT - 1;
            overflow = true;
        }
        Cursor::set_hw_pos(self.x, self.y);
        return overflow;
    }
}

pub struct Screen {
    buffer: &'static mut Buffer,
    cursor: Cursor,
    attribute: Attribute
}

#[allow(dead_code)]
impl Screen {

    pub fn new() -> Self {
        Self {
            buffer: unsafe { &mut*(0xB8000 as *mut Buffer) },
            cursor: Cursor::new(),
            attribute: Attribute::new(Color::White, Color::Black)
        }
    }

    fn scroll(&mut self) {
        for y in 1..SCREEN_HEIGHT {
            for x in 0..SCREEN_WIDTH {
                let symbol: Symbol = self.buffer.symbols[y][x];
                self.buffer.symbols[y-1][x] = symbol;
            }
        }
        for x in 0..SCREEN_WIDTH {
            self.buffer.symbols[SCREEN_HEIGHT-1][x] = Symbol::new(' ', self.attribute);
        }
    }

    fn linebreak(&mut self) {
        if self.cursor.newline() {
            self.scroll();
        }
    }

    pub fn putchar(&mut self, ch: char) {
        match ch as u8 {
            0x00 => { }
            0x0A => { self.linebreak(); }
            _ => {
                self.buffer.symbols[self.cursor.y][self.cursor.x] = Symbol::new(ch, self.attribute);
                if self.cursor.move_right() {
                    self.scroll();
                }
            }
        }
    }

    pub fn clear(&mut self) {
        for y in 0..SCREEN_HEIGHT {
            for x in 0..SCREEN_WIDTH {
                self.buffer.symbols[y][x] = Symbol::new(' ', self.attribute);
            }
        }
        self.cursor.set_pos(0, 0);
    }

    pub fn set_attribute(&mut self, attribute: Attribute) {
        self.attribute = attribute;
    }

    pub fn print(&mut self, msg: &str) {
        for ch in msg.chars() {
            self.putchar(ch);
        }
    }

    pub fn println(&mut self, msg: &str) {
        // TODO: optimize (move cursor at the end onve)
        self.print(msg);
        self.putchar('\n');
    }
}

impl fmt::Write for Screen {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        self.print(s);
        Ok(())
    }
}

lazy_static! {
    pub static ref SCREEN: Mutex<Screen> = Mutex::new(Screen::new());
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::io::cga::_print(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

#[doc(hidden)]
pub fn _print(args: fmt::Arguments) {
    use core::fmt::Write;
    SCREEN.lock().write_fmt(args).unwrap();
}