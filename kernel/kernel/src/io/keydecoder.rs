
const BREAK_BIT: u8 = 1 << 7;

struct ASCIIEntry {
    normal: char,
    shift: char,
    alt: char,
}

#[allow(dead_code)]
#[derive(Clone, Copy)]
pub enum KeyCode {
    KeyInvalid,         // 0x00
    KeyEscape,          // 0x01
    Key1,               // 0x02
    Key2,               // 0x03
    Key3,               // 0x04
    Key4,               // 0x05
    Key5,               // 0x06
    Key6,               // 0x07
    Key7,               // 0x08
    Key8,               // 0x09
    Key9,               // 0x0A
    Key0,               // 0x0B
    KeyDash,            // 0x0C
    KeyEqual,           // 0x0D
    KeyBackspace,       // 0x0E
    KeyTAB,             // 0x0F
    KeyQ,               // 0x10
    KeyW,               // 0x11
    KeyE,               // 0x12
    KeyR,               // 0x13
    KeyT,               // 0x14
    KeyY,               // 0x15
    KeyU,               // 0x16
    KeyI,               // 0x17
    KeyO,               // 0x18
    KeyP,               // 0x19
    KeyBracketsOpen,    // 0x1A
    KeyBracketsClose,   // 0x1B
    KeyEnter,           // 0x1C
    KeyLeftCtrl,        // 0x1D
    KeyA,               // 0x1E
    KeyS,               // 0x1F
    KeyD,               // 0x20
    KeyF,               // 0x21
    KeyG,               // 0x22
    KeyH,               // 0x23
    KeyJ,               // 0x24
    KeyK,               // 0x25
    KeyL,               // 0x26
    KeySemicolon,       // 0x27
    KeySingleQuote,     // 0x28
    KeyBacktick,        // 0x29
    KeyLeftShift,       // 0x2A
    KeyBackslash,       // 0x2B
    KeyZ,               // 0x2C
    KeyX,               // 0x2D
    KeyC,               // 0x2E
    KeyV,               // 0x2F
    KeyB,               // 0x30
    KeyN,               // 0x31
    KeyM,               // 0x32
    KeyComma,           // 0x33
    KeyPeriod,          // 0x34
    KeySlash,           // 0x35
    KeyRightShift,      // 0x36
    KeyNumStar,         // 0x37
    KeyLeftAlt,         // 0x38
    KeySpace,           // 0x39
    KeyCapsLock,        // 0x3A
    KeyF1,              // 0x3B
    KeyF2,              // 0x3C
    KeyF3,              // 0x3D
    KeyF4,              // 0x3E
    KeyF5,              // 0x3F
    KeyF6,              // 0x40
    KeyF7,              // 0x41
    KeyF8,              // 0x42
    KeyF9,              // 0x43
    KeyF10,             // 0x44
    KeyNumLock,         // 0x45
    KeyScrollLock,      // 0x46
    KeyNum7,            // 0x47
    KeyNum8,            // 0x48
    KeyNum9,            // 0x49
    KeyNumDash,         // 0x4A
    KeyNum4,            // 0x4B
    KeyNum5,            // 0x4C
    KeyNum6,            // 0x4D
    KeyNumPlus,         // 0x4E
    KeyNum1,            // 0x4F
    KeyNum2,            // 0x50
    KeyNum3,            // 0x51
    KeyNum0,            // 0x52
    KeyNumPeriod,       // 0x53
    KeyF11,             // 0x54
    KeyF12,             // 0x55
}

fn u8_to_keycode(keycode: u8) -> KeyCode {
    return match keycode {
        0x01 => { KeyCode::KeyEscape }
        0x02 => { KeyCode::Key1 }
        0x03 => { KeyCode::Key2 }
        0x04 => { KeyCode::Key3 }
        0x05 => { KeyCode::Key4 }
        0x06 => { KeyCode::Key5 }
        0x07 => { KeyCode::Key6 }
        0x08 => { KeyCode::Key7 }
        0x09 => { KeyCode::Key8 }
        0x0A => { KeyCode::Key9 }
        0x0B => { KeyCode::Key0 }

        0x2A => { KeyCode::KeyLeftShift }
        0x36 => { KeyCode::KeyRightShift }

        _ => { KeyCode::KeyInvalid }
    };
}

// TODO: assuming scan code set 1
static ASCII_TABLE: [ASCIIEntry; 0xF0] = [
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x00
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_ESCAPE    0x01
    ASCIIEntry { normal: '1', shift: '!', alt: '\0' },  // KEY_1    0x02
    ASCIIEntry { normal: '2', shift: '@', alt: '\0' },  // KEY_2    0x03
    ASCIIEntry { normal: '3', shift: '#', alt: '\0' },  // KEY_3    0x04
    ASCIIEntry { normal: '4', shift: '$', alt: '\0' },  // KEY_4    0x05
    ASCIIEntry { normal: '5', shift: '%', alt: '\0' },  // KEY_5    0x06
    ASCIIEntry { normal: '6', shift: '^', alt: '\0' },  // KEY_6    0x07
    ASCIIEntry { normal: '7', shift: '&', alt: '\0' },  // KEY_7    0x08
    ASCIIEntry { normal: '8', shift: '*', alt: '\0' },  // KEY_8    0x09
    ASCIIEntry { normal: '9', shift: '(', alt: '\0' },  // KEY_9    0x0A
    ASCIIEntry { normal: '0', shift: ')', alt: '\0' },  // KEY_0    0x0B
    ASCIIEntry { normal: '-', shift: '_', alt: '\0' },  // KEY_DASH 0x0C
    ASCIIEntry { normal: '=', shift: '+', alt: '\0' },  // KEY_EQUAL    0x0D
    ASCIIEntry { normal: '\x08', shift: '\x08', alt: '\0' },  // KEY_BACKSPACE    0x0E
    ASCIIEntry { normal: '\t', shift: '\t', alt: '\0' },  // KEY_TAB    0x0F
    ASCIIEntry { normal: 'q', shift: 'Q', alt: '\0' },  // KEY_Q    0x10
    ASCIIEntry { normal: 'w', shift: 'W', alt: '\0' },  // KEY_W    0x11
    ASCIIEntry { normal: 'e', shift: 'E', alt: '\0' },  // KEY_E    0x12
    ASCIIEntry { normal: 'r', shift: 'R', alt: '\0' },  // KEY_R    0x13
    ASCIIEntry { normal: 't', shift: 'T', alt: '\0' },  // KEY_T    0x14
    ASCIIEntry { normal: 'y', shift: 'Y', alt: '\0' },  // KEY_Y    0x15
    ASCIIEntry { normal: 'u', shift: 'U', alt: '\0' },  // KEY_U    0x16
    ASCIIEntry { normal: 'i', shift: 'I', alt: '\0' },  // KEY_I    0x17
    ASCIIEntry { normal: 'o', shift: 'O', alt: '\0' },  // KEY_O    0x18
    ASCIIEntry { normal: 'p', shift: 'P', alt: '\0' },  // KEY_P    0x19
    ASCIIEntry { normal: '[', shift: '{', alt: '\0' },  // KEY_BRACKETS_OPEN    0x1A
    ASCIIEntry { normal: ']', shift: '}', alt: '\0' },  // KEY_BRACKETS_CLOSE    0x1B
    ASCIIEntry { normal: '\n', shift: '\n', alt: '\0' },  // KEY_ENTER    0x1C
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_LEFT_CTRL    0x1D
    ASCIIEntry { normal: 'a', shift: 'A', alt: '\0' },  // KEY_A    0x1E
    ASCIIEntry { normal: 's', shift: 'S', alt: '\0' },  // KEY_S    0x1F
    ASCIIEntry { normal: 'd', shift: 'D', alt: '\0' },  // KEY_D    0x20
    ASCIIEntry { normal: 'f', shift: 'F', alt: '\0' },  // KEY_F    0x21
    ASCIIEntry { normal: 'g', shift: 'G', alt: '\0' },  // KEY_G    0x22
    ASCIIEntry { normal: 'h', shift: 'H', alt: '\0' },  // KEY_H    0x23
    ASCIIEntry { normal: 'j', shift: 'J', alt: '\0' },  // KEY_J    0x24
    ASCIIEntry { normal: 'k', shift: 'K', alt: '\0' },  // KEY_K    0x25
    ASCIIEntry { normal: 'l', shift: 'L', alt: '\0' },  // KEY_L    0x26
    ASCIIEntry { normal: ';', shift: ':', alt: '\0' },  // KEY_SEMICOLON    0x27
    ASCIIEntry { normal: '\'', shift: '"', alt: '\0' },  // KEY_SINGLEQUOTE    0x28
    ASCIIEntry { normal: '`', shift: '~', alt: '\0' },  // KEY_BACKTICK    0x29
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_LEFT_SHIFT 0x2A
    ASCIIEntry { normal: '\\', shift: '|', alt: '\0' },  // KEY_BACKSLASH    0x2B
    ASCIIEntry { normal: 'z', shift: 'Z', alt: '\0' },  // KEY_Z    0x2C
    ASCIIEntry { normal: 'x', shift: 'X', alt: '\0' },  // KEY_X    0x2D
    ASCIIEntry { normal: 'c', shift: 'C', alt: '\0' },  // KEY_C    0x2E
    ASCIIEntry { normal: 'v', shift: 'V', alt: '\0' },  // KEY_V    0x2F
    ASCIIEntry { normal: 'b', shift: 'B', alt: '\0' },  // KEY_B    0x30
    ASCIIEntry { normal: 'n', shift: 'N', alt: '\0' },  // KEY_N    0x31
    ASCIIEntry { normal: 'm', shift: 'M', alt: '\0' },  // KEY_M    0x32
    ASCIIEntry { normal: ',', shift: '<', alt: '\0' },  // KEY_COMMA    0x33
    ASCIIEntry { normal: '.', shift: '>', alt: '\0' },  // KEY_PERIOD    0x34
    ASCIIEntry { normal: '/', shift: '?', alt: '\0' },  // KEY_SLASH    0x35
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_RIGHT_SHIFT    0x36
    ASCIIEntry { normal: '*', shift: '\0', alt: '\0' },  // KEY_NUM_STAR    0x37
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_LEFT_ALT    0x38
    ASCIIEntry { normal: ' ', shift: ' ', alt: '\0' },  // KEY_SPACE    0x39
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_CAPS_LOCK    0x3A
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F1    0x3B
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F2    0x3C
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F3    0x3D
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F4    0x3E
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F5    0x3F
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F6    0x40
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F7    0x41
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F8    0x42
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F9    0x43
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F10    0x44
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_NUM_LOCK    0x45
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_SCROLL_LOCK    0x46
    ASCIIEntry { normal: '7', shift: '\0', alt: '\0' },  // KEY_NUM_7    0x47
    ASCIIEntry { normal: '8', shift: '\0', alt: '\0' },  // KEY_NUM_8    0x48
    ASCIIEntry { normal: '9', shift: '\0', alt: '\0' },  // KEY_NUM_9    0x49
    ASCIIEntry { normal: '-', shift: '\0', alt: '\0' },  // KEY_NUM_DASH    0x4A
    ASCIIEntry { normal: '4', shift: '\0', alt: '\0' },  // KEY_NUM_4    0x4B
    ASCIIEntry { normal: '5', shift: '\0', alt: '\0' },  // KEY_NUM_5    0x4C
    ASCIIEntry { normal: '6', shift: '\0', alt: '\0' },  // KEY_NUM_6    0x4D
    ASCIIEntry { normal: '+', shift: '\0', alt: '\0' },  // KEY_NUM_PLUS    0x4E
    ASCIIEntry { normal: '1', shift: '\0', alt: '\0' },  // KEY_NUM_1    0x4F
    ASCIIEntry { normal: '2', shift: '\0', alt: '\0' },  // KEY_NUM_2    0x50
    ASCIIEntry { normal: '3', shift: '\0', alt: '\0' },  // KEY_NUM_3    0x51
    ASCIIEntry { normal: '0', shift: '\0', alt: '\0' },  // KEY_NUM_0    0x52
    ASCIIEntry { normal: '.', shift: '\0', alt: '\0' },  // KEY_NUM_PERIOD    0x53
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F11    0x54
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_F12    0x55

    // TODO: implement
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x56
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x57
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x58
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x59
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x5A
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x5B
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x5C
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x5D
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x5E
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x5F
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x60
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x61
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x62
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x63
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x64
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x65
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x66
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x67
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x68
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x69
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x6A
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x6B
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x6C
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x6D
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x6E
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x6F
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x70
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x71
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x72
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x73
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x74
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x75
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x76
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x77
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x78
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x79
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x7A
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x7B
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x7C
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x7D
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x7E
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x7F
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x80
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x81
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x82
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x83
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x84
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x85
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x86
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x87
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x88
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x89
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x8A
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x8B
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x8C
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x8D
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x8E
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x8F
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x90
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x91
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x92
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x93
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x94
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x95
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x96
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x97
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x98
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x99
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x9A
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x9B
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x9C
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x9D
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x9E
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0x9F
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA0
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA1
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA2
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA3
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA4
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA5
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA6
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA7
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA8
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xA9
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xAA
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xAB
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xAC
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xAD
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xAE
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xAF
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB0
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB1
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB2
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB3
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB4
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB5
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB6
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB7
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB8
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xB9
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xBA
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xBB
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xBC
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xBD
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xBE
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xBF
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC0
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC1
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC2
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC3
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC4
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC5
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC6
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC7
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC8
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xC9
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xCA
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xCB
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xCC
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xCD
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xCE
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xCF
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD0
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD1
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD2
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD3
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD4
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD5
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD6
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD7
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD8
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xD9
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xDA
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xDB
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xDC
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xDD
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xDE
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xDF
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE0
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE1
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE2
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE3
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE4
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE5
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE6
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE7
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE8
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xE9
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xEA
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xEB
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xEC
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xED
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xEE
    ASCIIEntry { normal: '\0', shift: '\0', alt: '\0' },  // KEY_INVALID    0xEF
];

pub struct Key {
    pub ascii: char,
    pub pressed: bool
}

pub struct KeyDecoder {
    shift: bool,
    alt: bool
}

impl KeyDecoder {

    pub fn new() -> Self {
        Self {
            shift: false,
            alt: false
        }
    }

    fn u8_to_ascii(&self, keycode: u8) -> char {
        let ascii_entry: &ASCIIEntry = &ASCII_TABLE[keycode as usize];
        if self.shift {
            return ascii_entry.shift;
        }
        if self.alt {
            return ascii_entry.alt;
        }
        return ascii_entry.normal;
    }

    pub fn decode(&mut self, scancode: u8) -> Key {

        // TODO: some keys send prefixes

        let pressed: bool = scancode & BREAK_BIT == 0;
        let scancode: u8 = scancode & !BREAK_BIT;
        let keycode: KeyCode = u8_to_keycode(scancode);
        let ascii = self.u8_to_ascii(scancode);

        // TODO: handle more special keys
        match keycode {
            KeyCode::KeyLeftShift | KeyCode::KeyRightShift => {
                self.shift = pressed;
            }
            KeyCode::KeyLeftAlt => {
                self.shift = pressed;
            }
            _ => { }
        };

        Key {
            ascii,
            pressed
        }
    }
}