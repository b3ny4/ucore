
extern {
    fn cpuid_get_features() -> u32;
}


// TODO: use
#[allow(dead_code)]
pub enum Features {
    EdxFPU          = 1 << 0,  
    EdxVME          = 1 << 1,  
    EdxDE           = 1 << 2,  
    EdxPSE          = 1 << 3,  
    EdxTSC          = 1 << 4,  
    EdxMSR          = 1 << 5,  
    EdxPAE          = 1 << 6,  
    EdxMCE          = 1 << 7,  
    EdxCX8          = 1 << 8,  
    EdxAPIC         = 1 << 9,  
    EdxSEP          = 1 << 11, 
    EdxMTRR         = 1 << 12, 
    EdxPGE          = 1 << 13, 
    EdxMCA          = 1 << 14, 
    EdxCMOV         = 1 << 15, 
    EdxPAT          = 1 << 16, 
    EdxPSE36        = 1 << 17, 
    EdxPSN          = 1 << 18, 
    EdxCLFLUSH      = 1 << 19, 
    EdxDS           = 1 << 21, 
    EdxACPI         = 1 << 22, 
    EdxMMX          = 1 << 23, 
    EdxFXSR         = 1 << 24, 
    EdxSSE          = 1 << 25, 
    EdxSSE2         = 1 << 26, 
    EdxSS           = 1 << 27, 
    EdxHTT          = 1 << 28, 
    EdxTM           = 1 << 29, 
    EdxIA64         = 1 << 30,
    EdxPBE          = 1 << 31
}

pub struct CPU {

}

impl CPU {
    pub fn get_features() -> u32 {
        unsafe {
            return cpuid_get_features();
        }
    }
}