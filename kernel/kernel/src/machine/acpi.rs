
use lazy_static::lazy_static;
use spin::Mutex;

use crate::print;
use crate::println;

const RSDP_SIGNATURE: [u8; 8] = *b"RSD PTR "; // char is not 8 bit

#[repr(C, packed)]
struct XSDP {
    signature: [u8; 8], // char is not 8 bit
    checksum: u8,
    oem_id: [u8; 6], // char is not 8 bit
    revision: u8,
    rsdt_address: u32,

    length: u32,
    xsdt_address: u64,
    extended_checksum: u8,
    reserved: [u8; 3]
}

#[derive(Clone, Copy)]
#[repr(C, packed)]
struct SDTHeader {
    signature: [u8; 4],
    length: u32,
    revision: u8,
    checksum: u8,
    oem_id: [u8; 6],
    oem_table_id: [u8; 8],
    oem_revision: u32,
    creator_id: u32,
    creator_revision: u32
}

pub struct RSDT {
    header: SDTHeader,
    table_ptr: &'static [u32]
}

impl RSDT {
    fn new(rsdt_address: u32) -> Self {
        let header_ptr = rsdt_address as *const SDTHeader;
        let header = unsafe { *header_ptr };
        let sdt_ptr = unsafe { header_ptr.offset(1) as *const u32 };
        // TODO: /8 for XSDT
        let entries: usize = (header.length as usize - core::mem::size_of::<SDTHeader>()) / 4;
        let table_ptr: &'static [u32] = unsafe { core::slice::from_raw_parts(sdt_ptr, entries) };
        Self {
            header,
            table_ptr 
        }
    }
}

pub struct SDT {
    header: SDTHeader,
    content_ptr: *const ()
}

impl SDT {
    fn new(rsdt_address: u32) -> Self {
        let header_ptr = rsdt_address as *const SDTHeader;
        let header = unsafe { *header_ptr };
        let content_ptr = unsafe { header_ptr.offset(1) as *const () };
        Self {
            header,
            content_ptr
        }
    }
}

pub struct ACPIType {
    rsdt: RSDT
}

impl ACPIType {

    fn find_rsdp(start: u64, length: u64) -> Option<&'static XSDP> {

        // TODO: use multiboot header tags

        for block in (0..(length/8)).step_by(2) {
            let rsdp_ptr: *const XSDP = (start + 8*block) as *const XSDP;
            let rsdp_ref: &'static XSDP = unsafe { &*rsdp_ptr };

            // TODO: check validity (assumed for now)
            if &rsdp_ref.signature == &RSDP_SIGNATURE {
                return Some(rsdp_ref);
            }
        }
        None
    }

    pub fn new() -> Self {

        // TODO: use xsdp if available (revision > 0)
        // TODO: enable ACPI Mode if not done yet

        // prepare EDBA
        let edba_ptr: u16 = unsafe { *(0x40E as *const u16) };

        // search in EDBA
        let rsdp = ACPIType::find_rsdp(edba_ptr as u64, 1024);
        if rsdp.is_some() {
            let rsdt: RSDT = RSDT::new(rsdp.unwrap().rsdt_address);
            return Self { rsdt };
        }

        // search in MBA
        let rsdp = ACPIType::find_rsdp(0xE0000, 0xFFFFF-0xE0000);
        if rsdp.is_some() {
            let rsdt: RSDT = RSDT::new(rsdp.unwrap().rsdt_address);
            return Self { rsdt };
        }
        panic!("Could not find RSDP");
    }

    pub fn get_sdt(&self, signature: &str) -> Option<SDT> {
        println!("searching for {}...", signature);
        println!("reading {} enries", self.rsdt.table_ptr.len());
        for table in self.rsdt.table_ptr.iter() {
            let sdt : SDT = SDT::new(*table);
            // TODO: check validity
            let sig = match core::str::from_utf8(&sdt.header.signature) {
                Ok(s) => s,
                Err(_) => "Invalid UTF-8",
            };
            if signature == sig {
                // TODO: DO NOT DELETE! MAKE FUNCTION TO DUMP MEMO
                // let ptr: *const u8 = *table as *const u8;
                // let num_bytes = 0x2D;
                // let slice = unsafe { core::slice::from_raw_parts(ptr, num_bytes as usize) };
                // println!("Memory at address {:p}", ptr);
                // for byte in slice {
                //     print!("{:X} " , byte);
                // }
                // println!();
                // println!("Start should be {:x}", ptr as usize + 0x2C);
                return Some(sdt);
            }
            println!("Found wrong signature: {}", sig);
        }
        None
    }

}

#[derive(Clone, Copy)]
#[repr(C, packed)]
struct MADTEntryHeader {
    entry_type: u8,
    length: u8
}

pub struct MADT {
    madt: SDT
}

impl MADT {

    // TODO: refactor
    pub fn new(madt: SDT) -> Self {
        // TODO: check if table is APIC
        Self {
            madt
        }
    }

    pub fn test(&self) {
        println!("start MADT parsing");
        let mut numcores: u8 = 0;

        // TODO: mask 8259 PIC interrupts
        // TODO: continue

        let length: usize = self.madt.header.length as usize;
        let header_size: usize = core::mem::size_of::<SDTHeader>() as usize;
        let content_ptr: usize = self.madt.content_ptr as usize;
        let add_header: usize = 8;

        let start: usize = content_ptr + add_header;
        let end: usize = start + length - header_size - add_header;

        // println!("header_size: {:X}", header_size as usize);
        // println!("length: {:X}", length as usize);
        // println!("start: {:X}", start as usize);
        // println!("end: {:X}", end as usize);

        let mut next: usize = start;

        while next < end {
            //print!("N: {:x} ", next);
            let madt_entry_ptr = next as *const MADTEntryHeader;
            let madt_entry = unsafe { *madt_entry_ptr };
            //print!("T: {} ", madt_entry.entry_type);
            //println!("L: {}", madt_entry.length);

            if madt_entry.entry_type == 0 {
                numcores += 1;
            }

            next = next + madt_entry.length as usize;
        }

        println!("Found {} Cores!", numcores);

    }

}

lazy_static! {
    pub static ref ACPI: Mutex<ACPIType> = Mutex::new(ACPIType::new());
}
