#![no_std]

mod machine {
    pub mod cpu;
    pub mod acpi;
}

mod io {
    pub mod cga;
    pub mod ioport;
    pub mod keydecoder;
}

use io::cga::Color;
use io::cga::Attribute;
use io::cga::SCREEN;
use io::ioport::IOPort;
use io::keydecoder::Key;
use io::keydecoder::KeyDecoder;
use machine::cpu::CPU;
use machine::acpi::SDT;
use machine::acpi::MADT;
use machine::acpi::ACPI;

pub const COLORS: [Color; 16] = [
        Color::Black,
        Color::Blue,
        Color::Green,
        Color::Cyan,
        Color::Red,
        Color::Magenta,
        Color::Brown,
        Color::LightGrey,
        Color::DarkGrey,
        Color::LightBlue,
        Color::LightGreen,
        Color::LightCyan,
        Color::LightRed,
        Color::LightMagenta,
        Color::Yellow,
        Color::White
];

pub const BG_COLORS: [Color; 9] = [
    Color::Black,
    Color::Blue,
    Color::Green,
    Color::Cyan,
    Color::Red,
    Color::Magenta,
    Color::Brown,
    Color::LightGrey,
    Color::DarkGrey
];

#[no_mangle]
pub extern "C" fn kernel_main() {

    SCREEN.lock().clear();

    println!("Hello World!");

    // for color in COLORS {
    //     SCREEN.lock().set_attribute(Attribute::new(color, Color::Black));
    //     print!("{}", 3 as char);
    // }

    // for color in BG_COLORS {
    //     SCREEN.lock().set_attribute(Attribute::new(Color::LightGrey, color));
    //     print!("{}", 3 as char);
    // }
    
    println!("");

    if (CPU::get_features() & (1 << 9)) == 0 {
        panic!("APIC not supported!");
    }

    let madt: SDT = ACPI.lock().get_sdt("APIC").expect("MADT not found!");

    let madt: MADT = MADT::new(madt);

    madt.test();

    // let ctrl_port: IOPort = IOPort::new(0x64);
    // let data_port: IOPort = IOPort::new(0x60);
    // let mut key_decoder: KeyDecoder = KeyDecoder::new();
    // loop {
    //     if (ctrl_port.recv() & 0x1) != 0 {
    //         // TODO: might be mouse
    //         let scancode: u8 = data_port.recv();
    //         let key: Key = key_decoder.decode(scancode);
    //         if key.pressed {
    //         print!("{}", key.ascii);
    //         }
    //     }
    // }

    loop {}

}

#[panic_handler]
fn panic(info: &core::panic::PanicInfo) -> ! {
    println!("{info}");
    loop {}
}
