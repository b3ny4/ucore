TARGET=x86_64

TARGET_BOOT_SOURCE_FILES := $(shell find kernel/arch/$(TARGET) -name *.S)
TARGET_BOOT_OBJECT_FILES := $(patsubst kernel/arch/$(TARGET)/%.S, build/arch/$(TARGET)/%.o, $(TARGET_BOOT_SOURCE_FILES))

.PHONY: all clean build/libkernel.a

all: build/uCore.iso
	echo $(TARGET_BOOT_OBJECT_FILES)

clean:
	rm -rf build
	rm -rf kernel/kernel/target

$(TARGET_BOOT_OBJECT_FILES): build/arch/$(TARGET)/%.o: kernel/arch/$(TARGET)/%.S
	mkdir -p $(dir $@)
	nasm -f elf64 $^ -o $@

build:
	mkdir -p build

build/uCore.iso: build build/iso build/kernel
	cp build/kernel build/iso/boot/kernel
	grub-mkrescue /usr/lib/grub/i386-pc -o build/uCore.iso build/iso

build/iso: kernel/iso
	cp -r kernel/iso build/iso

build/kernel: build build/libkernel.a $(TARGET_BOOT_OBJECT_FILES)
	$(TARGET)-elf-ld -n -o $@ -T kernel/arch/$(TARGET)/linker.ld $(TARGET_BOOT_OBJECT_FILES) -L./build -lkernel

build/libkernel.a: build
	cd kernel/kernel && \
	cargo xbuild --target $(TARGET).json
	cp kernel/kernel/target/$(TARGET)/debug/libkernel.a $@

qemu: build/uCore.iso
	qemu-system-$(TARGET) -smp 4 -cdrom build/uCore.iso

qemu-gdb: all
	gdb build/kernel \
		-ex "set arch i386:x86-64" \
		-ex "target remote | exec qemu-system-x86_64 -gdb stdio build/kernel -smp 4 -cdrom build/uCore.iso"